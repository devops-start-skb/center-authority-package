# Center Authority package

### Notes
* Organisation settings declare in ``/home/ca/easy-rsa/vars`` file

## Create package

```shell
dpkg-deb --build ca-my-org
```

## Transfer to target (scp example)
```
scp ca-my-org.deb debian@192.168.3.56:~
```


## Install (need superuser privileges)

```shell
dpkg -i ca-my-org.deb
```

## Remove

```shell (need superuser privileges)
dpkg -r ca-my-org
```